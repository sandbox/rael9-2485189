/**
 * @file
 *   Adds or removes a node to/from a collection, and animates the response, updating the list.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.nodeOrganizerAddRemoveBehavior = {
    attach: function (context, settings) {
      $('.node-organizer-add-to-list a').each(function() {
        $(this).click(function(event) {
          event.preventDefault();
          $.getJSON($(this).attr('href'), { json: true }, function(data) {
            $('.node-organizer-add-to-list').fadeOut('slow', function() {
              $(this).html(data.message);
              $(this).fadeIn('slow').delay('slow').fadeOut('slow', function() {
                $(this).replaceWith(data.list);
                Drupal.attachBehaviors('.node-organizer-add-to-list');
                $(this).fadeIn('slow');
              });
            });
          });
        });
      });
    }
  };
}(jQuery, Drupal, drupalSettings));
