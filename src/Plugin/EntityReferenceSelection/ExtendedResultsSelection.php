<?php

namespace Drupal\node_organizer\Plugin\EntityReferenceSelection;


use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;

/**
 * Entity reference selection that extends the number of results.
 *
 * @EntityReferenceSelection(
 *   id = "node_organizer:node",
 *   label = @Translation("Node Organizer node"),
 *   group = "node_organizer",
 * )
 */
class ExtendedResultsSelection extends NodeSelection {

  /**
   * @inheritdoc
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    return parent::getReferenceableEntities($match, $match_operator, 100);
  }
}
