<?php
/**
* @file
* Contains \Drupal\node_organizer\Plugin\Block\NodeOrganizerLinearNavBlock.
*/

namespace Drupal\node_organizer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node_organizer\NodeOrganizerUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;
//use Drupal\Core\Link;


/**
* Provides a 'Node Organizer personal list' block.
*
* @Block(
*   id = "node_organizer_add_to_list_block",
*   admin_label = @Translation("Node Organizer Add To List"),
*   category = @Translation("Node Organizer")
* )
*/
class NodeOrganizerAddToListBlock extends BlockBase implements ContainerFactoryPluginInterface {
  protected $utilities;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, NodeOrganizerUtilities $utilities) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->utilities = $utilities;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('node_organizer.utilities')
    );
  }

  /**
  * {@inheritdoc}
  */
  public function build() {
    $block = [];
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node) {
      $block = $this->utilities->addToList($node);
    }
    return $block;
  }
}
