<?php
/**
* @file
* Contains \Drupal\node_organizer\Plugin\Block\NodeOrganizerLinearNavBlock.
*/

namespace Drupal\node_organizer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node_organizer\NodeOrganizerStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
* Provides a 'Node Organizer personal list' block.
*
* @Block(
*   id = "node_organizer_personal_list_block",
*   admin_label = @Translation("Node Organizer Personal List"),
*   category = @Translation("Node Organizer")
* )
*/
class NodeOrganizerPersonalListBlock extends BlockBase implements ContainerFactoryPluginInterface {
  protected $storage;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, NodeOrganizerStorage $storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->storage = $storage;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('node_organizer.storage')
    );
  }

  /**
  * {@inheritdoc}
  */
  public function build() {
    $user = \Drupal::currentUser();

    // Get the list of collections for this user.
    $collections = $this->storage->getCollectionList(['uid' => $user->id()]);

    // Common list settings.
    $list = [
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#attributes' => [
        'id' => 'node-organizer-personal-list-' . $user->id(),
        'class' => ['node-organizer-personal-list'],
      ],
      '#items' => [],
    ];

    // For each collection, add it to the renderable list.
    foreach ($collections as $noid => $name) {
      $list['#items'][]['#markup'] = Link::createFromRoute($name, 'node_organizer.collection', ['collection' => $noid])->toString()->getGeneratedLink();
    }

    // @TODO: See if we can set up a good sane caching setup instead of not caching this block at all.
    return [
      'subject' => NULL,
      'content' => $list,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
