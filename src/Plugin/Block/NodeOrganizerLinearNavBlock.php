<?php
/**
* @file
* Contains \Drupal\node_organizer\Plugin\Block\NodeOrganizerLinearNavBlock.
*/

namespace Drupal\node_organizer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;


/**
* Provides a 'Node Organizer linear navigation' block.
*
* @Block(
*   id = "node_organizer_linear_nav_block",
*   admin_label = @Translation("Node Organizer Previous / Top / Next Navigation"),
*   category = @Translation("Node Organizer")
* )
*/
class NodeOrganizerLinearNavBlock extends BlockBase {

  /**
  * {@inheritdoc}
  */
  public function build() {
    $block = [];
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node && $node->node_organizer) {
      if (!$node->node_organizer->getCurrentObject()) {
        $collections = $node->node_organizer->nodeLookup($node->id(), 'system');
        if ($collections) {
          $node->node_organizer->loadCollection($collections[0]);
        }
        $node->node_organizer->setCurrentObject($node->id());
      }

      // Common list settings.
      $list = [
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#attributes' => [
          'id' => 'node-organizer-linear-nav-' . $node->node_organizer->getCollectionId(),
          'class' => 'node-organizer-linear-nav',
        ],
      ];

      // Get the previous, next and top nodeOrganizerItem objects.
      $previous = $node->node_organizer->getPrevious();
      $next = $node->node_organizer->getNext();
      $top = $node->node_organizer->getTop();

      $list['#items'] = [];

      // Create the Previous navigation item.
      if ($previous) {
        $url = Url::fromRoute(
          'entity.node.canonical',
          ['node' => $previous->nid],
          [
            'attributes' => [
              'class' => [
                'node-organizer-previous-link'
              ],
            ],
          ]);
        $list['#items'][]['#markup'] = Link::fromTextAndUrl(
          Markup::create('<span class="node-organizer-previous">' . t('Previous') . '</span><span class="node-organizer-previous-title">' . $previous->getTitle() . '</span>'),
          $url
        )->toString()->getGeneratedLink();
      }
      else {
        $list['#items'][]['#markup'] = '<span class="node-organizer-blank">&nbsp;</span>';
      }

      // Create the Top navigation item.
      $url = Url::fromRoute(
        'entity.node.canonical',
        ['node' => $top->nid],
        [
          'attributes' => [
            'class' => ['node-organizer-top-link'],
          ],
        ]
      );
      $list['#items'][]['#markup'] = Link::fromTextAndUrl(
        Markup::create('<span class="node-organizer-top-title">' . $top->getTitle() . '</span>'),
        $url
      )->toString()->getGeneratedLink();

      // Create the Next navigation item.
      if ($next) {
        $url = Url::fromRoute(
          'entity.node.canonical',
          ['node' => $next->nid],
          [
            'attributes' => [
              'class' => ['node-organizer-next-link'],
            ],
          ]
        );
        $list['#items'][]['#markup'] = Link::fromTextAndUrl(
          Markup::create('<span class="node-organizer-next">' . t('Next') . '</span><span class="node-organizer-next-title">' . $next->getTitle() . '</span>'),
          $url
        )->toString()->getGeneratedLink();
      }
      else {
        $list['#items'][]['#markup'] = '<span class="node-organizer-blank">&nbsp;</span>';
      }

      // Return the renderable list array.
      $block = [
        'subject' => NULL,
        'content' => [
          'list' => $list,
          '#attached' => [
            'library' => [
              'node_organizer/linear-nav-block',
            ],
          ],
        ],
      ];
    }
    return $block;
  }

  // @TODO: See if we can set up a good sane caching setup instead of having people clear cache after updating collections.
  public function getCacheTags() {
    // When the node changes, rebuild block.
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      // If there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    // Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }
}
