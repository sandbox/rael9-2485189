<?php
/**
* @file
* Contains \Drupal\node_organizer\Plugin\Block\NodeOrganizerLinearNavBlock.
*/

namespace Drupal\node_organizer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node_organizer\NodeOrganizerUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
* Provides a 'Node Organizer tree navigation' block.
*
* @Block(
*   id = "node_organizer_tree_nav_block",
*   admin_label = @Translation("Node Organizer Tree Navigation"),
*   category = @Translation("Node Organizer")
* )
*/
class NodeOrganizerTreeNavBlock extends BlockBase implements ContainerFactoryPluginInterface {
  protected $utilities;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, NodeOrganizerUtilities $utilities) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->utilities = $utilities;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('node_organizer.utilities')
    );
  }
  /**
  * {@inheritdoc}
  */
  public function build() {
    $block = [];
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node && $node->node_organizer) {
      if (!$node->node_organizer->getCurrentObject()) {
        $collections = $node->node_organizer->nodeLookup($node->id(), 'system');
        if ($collections) {
          $node->node_organizer->loadCollection($collections[0]);
        }
      }
      // Common list settings.
      $list = [
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#attributes' => [
          'id' => 'node-organizer-tree-nav-' . $node->node_organizer->getCollectionId(),
          'class' => [
            'node-organizer-tree-nav',
          ],
        ],
        '#items' => [],
        '#attached' => [
          'library' => [
            'node_organizer/tree-nav-block',
          ],
        ],
      ];

      // Recurse through the collection, creating the tree in a list.
      $this->utilities->recurseCollection($node->node_organizer->getCollection(), $list['#items'], $node->id());

      // Return the renderable list array.
      $block = ['subject' => NULL, 'content' => $list];
    }
    return $block;
  }

  // @TODO: See if we can set up a good sane caching setup instead of having people clear cache after updating collections.
  public function getCacheTags() {
    // When the node changes, rebuild block.
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      // If there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    // Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }
}
