<?php


namespace Drupal\node_organizer\Form;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node_organizer\NodeOrganizer;
use Drupal\node_organizer\NodeOrganizerItem;
use Drupal\node_organizer\NodeOrganizerStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp;


class NodeOrganizerImportForm extends FormBase {
  protected $node_organizer;
  protected $storage;
  protected $entity_repository;

  public function __construct(NodeOrganizer $node_organizer, NodeOrganizerStorage $storage, EntityRepositoryInterface $entity_repository) {
    $this->node_organizer = $node_organizer;
    $this->storage = $storage;
    $this->entity_repository = $entity_repository;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('node_organizer'),
      $container->get('node_organizer.storage'),
      $container->get('entity.repository')
    );
  }

  public function getFormId() {
    return 'node_organizer_import';
  }

  /**
   * Form for importing node_organizer collections.
   *
   * @param $form
   * @param $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = array();

    $form['replace'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Replace current collections?'),
      '#description' => $this->t('If checked, all current collections will be replaced with the imported collections.'),
    );

    $form['data'] = array(
      '#type' => 'textarea',
      '#title' => t('Collection Data'),
      '#description' => t('The JSON-formatted collection data from the source site.'),
      '#required' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Import'),
    );

    /**
     * End form building
     */

    // Return completed form
    return $form;
  }

  /**
   * Validation handler for node_organizer_collection_import form.
   *
   * @param $form
   * @param $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $json = $form_state->getValue('data');
      $decoded = GuzzleHttp\json_decode($json);
    } catch (\InvalidArgumentException $e) {
      $form_state->setErrorByName('data', $this->t('JSON was not valid: @error', ['@error' => $e]));
    }
  }

  /**
   * Submission handler for node_organizer_collection_import form.
   *
   * @param $form
   * @param $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $json = $form_state->getValue('data');
    $decoded = GuzzleHttp\json_decode($json);
    $replace = $form_state->getValue('replace');

    if ($replace) {
      $collections = $this->storage->getCollectionList();
      foreach ($collections as $noid => $name) {
        $this->node_organizer->loadCollection($noid);
        $this->node_organizer->deleteCollection();
      }
    }

    try {
      foreach ($decoded as $noid => $data) {
        $collection = [];
        $this->recurseStructure($data->structure, $collection);
        $user = $this->entity_repository->loadEntityByUuid('user', $data->user_uuid);
        if ($user) {
          $user_id = $user->id();
        } else {
          $user_id = 1;
        }
        $this->node_organizer->newCollection($collection, $data->type, $data->name, $user_id);
      }

      drupal_set_message($this->t('The collections were imported successfully.'));
      $form_state->setRedirect('node_organizer.list');
    } catch (\Exception $e) {
      drupal_set_message($this->t('There was an error during import: @error', ['@error' => $e->getMessage()]), 'error');
    }
  }

  /**
   * Recurses through the imported collection structure to turn it back into the
   * internal structure.
   *
   * @param $current_level
   * @param $structure
   *
   * @throws \Exception
   */
  private function recurseStructure($current_level, &$structure) {
    if (is_array($current_level)) {
      // Go through each object at this level.
      foreach($current_level as $data) {
        $item = new NodeOrganizerItem();
        if ($data->type == 'node') {
          $node = $this->entity_repository->loadEntityByUuid('node', $data->uuid);
          if ($this->node_organizer->nodeLookup($node->id(), 'system')) {
            throw new \Exception($this->t('Node already used in another system collection: @id - @title.', ['@id' => $node->id(), '@title' => $data->title]));
          }
          $item->nid = $node->id();
        } else {
          $item->fid = $data->id;
        }
        $item->title = $data->title;
        $item->weight = $data->weight;
        $item->depth = $data->depth;
        if ($data->parent > 0) {
          $node = $this->entity_repository->loadEntityByUuid('node', $data->parent_uuid);
          $item->parent = $node->id();
        } else {
          $item->parent = $data->parent;
        }

        // If this item has children, add an array for the children, and recurse.
        if (isset($data->children) && is_array($data->children)) {
          $item->children = [];
          $this->recurseStructure($data->children, $item->children);
        }
        $structure[] = $item;
      }
    }
  }

}
