<?php


namespace Drupal\node_organizer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node_organizer\NodeOrganizerUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp;


class NodeOrganizerExportForm extends FormBase {
  protected $utilities;

  public function __construct(NodeOrganizerUtilities $utilities) {
    $this->utilities = $utilities;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('node_organizer.utilities')
    );
  }

  public function getFormId() {
    return 'node_organizer_export';
  }

  /**
   * Form for exporting node_organizer collections.
   *
   * @param $form
   * @param $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $json_data = $this->utilities->getCollectionsExport(FALSE);

    $form = array();

    $form['export'] = array(
      '#type' => 'textarea',
      '#title' => t('Collection Data'),
      '#description' => t('The JSON-formatted collection data.'),
      '#default_value' => GuzzleHttp\json_encode($json_data, JSON_PRETTY_PRINT),
      '#rows' => '30',
    );

    // Return completed form
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do nothing. This form is just there to hold the JSON.
  }

}
