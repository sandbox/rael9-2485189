<?php

/**
 * Created by IntelliJ IDEA.
 * User: jbaker
 * Date: 5/11/17
 * Time: 1:11 PM
 */

namespace Drupal\node_organizer\Form;

use Drupal\Core\Render\RendererInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node_organizer\NodeOrganizer;
use Drupal\node_organizer\NodeOrganizerItem;
use Drupal\node_organizer\NodeOrganizerUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;


class NodeOrganizerEditForm extends FormBase {
  protected $node_organizer;
  protected $utilities;
  protected $renderer;

  public function __construct(NodeOrganizer $node_organizer, NodeOrganizerUtilities $utilities, RendererInterface $renderer) {
    $this->node_organizer = $node_organizer;
    $this->utilities = $utilities;
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('node_organizer'),
      $container->get('node_organizer.utilities'),
      $container->get('renderer')
    );
  }

  public function getFormId() {
    return 'node_organizer_edit';
  }

  /**
   * Form for creating/editing node_organizer collections.
   *
   * @param $form
   * @param $form_state
   * @param null $collection
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $collection = NULL) {
    $account = \Drupal::currentUser();
    /**
     * Start form_state setup
     */
    $node_organizer = array();
    // If the collection isn't already created/stored/loaded, do so
    $node_organizer['collection'] = array();
    if ($collection !== NULL) {
      $node_organizer['collection_id'] = $collection;
      $node_organizer['collection'] = $this->node_organizer->loadCollection($collection);
    }

    // Get the collection types and make them ready for display/storage
    $types = $this->node_organizer->getTypes();
    $translated_types = array();
    foreach ($types as $type) {
      $translated_types[$type] = t(ucfirst($type));
    }
    $node_organizer['types'] = $translated_types;

    // Store the initial folder ID value
    $node_organizer['folder_val'] = $this->node_organizer->nextFolder();

    // Store the initial weight
    $node_organizer['next_weight'] = -50;
    if (is_array($node_organizer['collection']) && count($node_organizer['collection'])) {
      $last_item = end($node_organizer['collection']);
      $node_organizer['next_weight'] = $last_item->weight + 1;
    }

    // Store whether this is an add or update form
    if (\Drupal::routeMatch()->getRouteName() == 'node_organizer.add') {
      $node_organizer['state'] = 'add';
    }
    else {
      $node_organizer['state'] = 'update';
    }

    // Set all the changes we made.
    $form_state->set('node_organizer', $node_organizer);

    /**
     * End form_state setup
     */

    /**
     * Start form building
     */

    $form = array();

    $form['#attached']['library'][] = 'node_organizer/edit';

    $form['container'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="collection-wrapper">',
      '#suffix' => '</div>',
    );

    $form['container']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Collection Name'),
      '#description' => t('The name you want to identify this collection with.'),
      '#maxlength' => 255,
      '#default_value' => $this->node_organizer->getName() ? $this->node_organizer->getName() : '',
      '#required' => TRUE,
    );

    $form['container']['type'] = array(
      '#type' => 'radios',
      '#title' => t('Collection Type'),
      '#description' => t('Allows selection of the type of collection. System is a system wide collection; nodes can only be added to one such collection. Personal collections are for a particular user, and the navigation for these will not be available on nodes directly.'),
      '#options' => $translated_types,
      '#default_value' => $this->node_organizer->getType() ? $this->node_organizer->getType() : NULL,
      '#access' => $account->hasPermission('edit node organizer system collections'),
      '#required' => TRUE,
    );

    if (!$account->hasPermission('edit node organizer system collections')) {
      $form['container']['type'] = array(
        '#type' => 'value',
        '#value' => 'personal',
      );
    }

    if (count($node_organizer['collection'])) {
      $header = array(t('Title'), t('Weight'), array('data' => t('Operations'), 'colspan' => '2'));
    } else {
      $header = array(t('Title'), array('data' => t('Operations'), 'colspan' => '2'));
    }

    $form['container']['collection-table'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#tabledrag' => array(
        array(
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'collection-plid',
          'subgroup' => 'collection-plid',
          'source' => 'collection-mlid',
          'hidden' => TRUE,
        ),
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'collection-weight',
        ),
      ),
      '#attributes' => array(
        'id' => 'collection-table'
      ),
      '#empty' => t("There aren't any nodes or folders yet.")
    );

    // Build the hierarchical collection table
    $this->listCreate($node_organizer['collection'], $form['container']['collection-table']);

    $form['container']['add_node_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Node'),
      '#attributes' => array(
        'class' => array(
          'add-node-wrapper',
        ),
      ),
    );

    $form['container']['add_node_fieldset']['node'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#selection_handler' => 'node_organizer:node',
      '#attributes' => array(
        'placeholder' => t('Node')
      ),
    );

    $form['container']['add_node_fieldset']['add_node'] = array(
      '#type' => 'submit',
      '#value' => t('Add Node'),
      '#submit' => array('::addNode'),
    );

    $form['container']['add_folder_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Folder'),
      '#attributes' => array(
        'class' => array(
          'add-folder-wrapper',
        ),
      ),
    );

    $form['container']['add_folder_fieldset']['folder'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#attributes' => array(
        'placeholder' => t('Folder Name')
      ),
    );

    $form['container']['add_folder_fieldset']['add_folder'] = array(
      '#type' => 'submit',
      '#value' => t('Add Folder'),
      '#submit' => array('::addFolder'),
    );

    $form['container']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );

    if (is_numeric($collection)) {
      $form['container']['delete'] = array(
        '#type' => 'markup',
        '#markup' => Link::createFromRoute(t('Delete'), 'node_organizer.delete', ['collection' => $collection], array('attributes' => array('class' => array('btn', 'btn-danger'))))->toString(),
      );
    }
    /**
     * End form building
     */

    // Return completed form
    return $form;
  }

  /**
   * Validation handler for node_organizer_collection_edit form.
   *
   * @param $form
   * @param $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $node_organizer = $form_state->get('node_organizer');

    // Check the type against allowed values.
    $types = $node_organizer['types'];
    $type = $form_state->getValue('type');
    if (!isset($types[$type])) {
      $form_state->setErrorByName('type', t('An incorrect value was set on this field.'));
    }

    // Get some basic values set up
    $collection = $form_state->getValue('collection-table');
    $first = False;
    if (is_array($collection)) {
      $first = reset($collection);
    }
    if ($type == 'system' && $first && isset($first['hidden']['fid']) && $first['hidden']['fid'] != '') {
      $form_state->setErrorByName('collection-table[' . $first['hidden']['fid'] . '][title]', t('The first item in system collections must be a node.'));
    }

    $trigger = $form_state->getTriggeringElement();
    switch ($trigger['#attributes']['data-drupal-selector']){
      case 'edit-submit':
        // Any full form submission validation.
        break;
      case 'edit-add-node':
        // Any Add Node specific validation
        // Check to see if there is a valid node id in the submitted value
        if (preg_match("/^\d+$/", $form_state->getValue('node', ''))) {
          // Check for some error conditions
          $error = FALSE;

          // Parse the field for the nid
          $nid = intval($form_state->getValue('node'));

          if (isset($node_organizer['collection_id'])) {
            $this->node_organizer->loadCollection($node_organizer['collection_id']);

            if (in_array($nid, $this->node_organizer->getNodes())) {
              $error = 'Node is already in this collection.';
            }
          }

          if ($form_state->getValue('type') == 'system' && $this->node_organizer->nodeLookup($nid, 'system')) {
            $error = 'Node already used in another System collection.';
          }
          if (isset($node_organizer['collection']) && count($node_organizer['collection']) == 100) {
            $error = 'Maximum number of top-level items in collection reached.';
          }

          if ($error) {
            $form_state->setErrorByName('node', t($error));
          }
        }
        else {
          $form_state->setErrorByName('node', t('This is not a valid node.'));
        }
        break;
      case 'edit-add-folder':
        // Any Add Folder specific validation
        $error = FALSE;
        if (isset($node_organizer['collection']) && count($node_organizer['collection']) == 100) {
          $error = 'Maximum number of top-level items in collection reached.';
        }

        if ($error) {
          $form_state->setErrorByName('folder', t($error));
        }
        break;
    }
  }

  /**
   * Submission handler for node_organizer_collection_edit form.
   *
   * @param $form
   * @param $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node_organizer = $form_state->get('node_organizer');
    // Get some basic values set up
    $collection = $form_state->getValue('collection-table');
    $sorter = [];

    // Iterate through the submitted collection
    foreach ($collection as $item) {
      // Get the parent ID, or default it
      $plid = intval($item['hidden']['plid']);
      if (!$plid) {
        $plid = 0;
      }

      // Create a new collection object, and add the required values to it
      $object = new NodeOrganizerItem();
      if (intval($item['hidden']['nid'])) {
        $object->nid = intval($item['hidden']['nid']);
      }
      if (intval($item['hidden']['fid'])) {
        $object->fid = intval($item['hidden']['fid']);
        $object->title = $item['title'];
      }
      $id = $object->getId();
      $object->weight = intval($item['hidden']['weight']);
      if ($plid) {
        $object->parent = $plid;
      }

      // Add the collection objects to an array keyed by parents, so we can group them
      $sorter[$plid][$id] = $object;
    }

    // Create the final array of objects, starting with the top-level objects (plid 0)
    $final_list = [];
    if (count($sorter)) {
      foreach ($sorter[0] as $id => $object) {
        $object->depth = 0;
        $final_list[$id] = $object;
      }
    }

    // Recurse through the array, adding the children to the correct parent objects
    $this->recurseFormItems($sorter, $final_list, $form_state);

    // Get some basic values set up
    $this->node_organizer->loadCollection($node_organizer['collection_id']);
    $name = $form_state->getValue('name');
    $type = $form_state->getValue('type');

    // If this is a pre-existing collection, update it, otherwise create it
    if (isset($node_organizer['state']) && $node_organizer['state'] == 'update') {
      $result = $this->node_organizer->updateCollection($final_list, $type, $name);
    }
    elseif (isset($node_organizer['state']) && $node_organizer['state'] == 'add') {
      $result = $this->node_organizer->newCollection($final_list, $type, $name);
    }
    else {
      $result = FALSE;
    }

    if ($result) {
      // Redirect to the edit version of this form
      $form_state->setRedirect('node_organizer.edit', ['collection' => $this->node_organizer->getCollectionId()]);
      drupal_set_message(t('Successfully saved collection %collection.', array('%collection' => $this->node_organizer->getName())));
    }
    else {
      drupal_set_message(t('There was an issue saving collection %collection', array('%collection' => $this->node_organizer->getName())));
    }
  }

  /**
   * Submit handler for adding a node
   *
   * @param $form
   * @param $form_state
   */
  function addNode(array &$form, FormStateInterface $form_state) {
    $node_organizer = $form_state->get('node_organizer');
    $nid = $form_state->getValue('node');
    $node = Node::load($nid);

    if ($node) {
      // Get the next weight
      $weight = $node_organizer['next_weight'];
      // Iterate the next_weight
      $node_organizer['next_weight']++;

      // Create a new object with the required fields
      $object = new NodeOrganizerItem();
      $object->nid = $node->id();
      $object->depth = 0;
      $object->weight = $weight;

      // Add the new object to the right spot in the form_state
      $node_organizer['collection'][$node->id()] = $object;
      $form_state->set('node_organizer', $node_organizer);

      // Get some basic values set up
      if (isset($node_organizer['collection_id'])) {
        $this->node_organizer->loadCollection($node_organizer['collection_id']);
      }
      $name = $form_state->getValue('name');
      $type = $form_state->getValue('type');
      $final_list = $node_organizer['collection'];

      // If this is a pre-existing collection, update it, otherwise create it
      if (isset($node_organizer['state']) && $node_organizer['state'] == 'update') {
        $result = $this->node_organizer->updateCollection($final_list, $type, $name);
      }
      elseif (isset($node_organizer['state']) && $node_organizer['state'] == 'add') {
        $result = $this->node_organizer->newCollection($final_list, $type, $name);
      }
      else {
        $result = FALSE;
      }

      if ($result) {
        // Redirect to the edit version of this form
        $form_state->setRedirect('node_organizer.edit', ['collection' => $this->node_organizer->getCollectionId()]);
        drupal_set_message(t('Successfully saved collection %collection.', array('%collection' => $this->node_organizer->getName())));
      }
      else {
        drupal_set_message(t('There was an issue saving collection %collection', array('%collection' => $this->node_organizer->getName())));
      }
    }
  }

  /**
   * Submit handler for adding a folder
   *
   * @param $form
   * @param $form_state
   */
  function addFolder(array &$form, FormStateInterface $form_state) {
    $node_organizer = $form_state->get('node_organizer');

    // Get the next weight
    $weight = $node_organizer['next_weight'];
    // Iterate the next_weight
    $node_organizer['next_weight']++;

    // Grab some values, and iterate the folder ID
    $folder = $form_state->getValue('folder');
    $current_folder_id = $node_organizer['folder_val'];
    $node_organizer['folder_val']--;

    // Create a new object with the required fields
    $object = new NodeOrganizerItem();
    $object->title = $folder;
    $object->fid = $current_folder_id;
    $object->depth = 0;
    $object->weight = $weight;

    // Add the new object to the right spot in the form_state
    $node_organizer['collection'][$current_folder_id] = $object;
    $form_state->set('node_organizer', $node_organizer);

    // Get some basic values set up
    $this->node_organizer->loadCollection($node_organizer['collection_id']);
    $name = $form_state->getValue('name');
    $type = $form_state->getValue('type');
    $final_list = $node_organizer['collection'];

    // If this is a pre-existing collection, update it, otherwise create it
    if (isset($node_organizer['state']) && $node_organizer['state'] == 'update') {
      $result = $this->node_organizer->updateCollection($final_list, $type, $name);
    }
    elseif (isset($node_organizer['state']) && $node_organizer['state'] == 'add') {
      $result = $this->node_organizer->newCollection($final_list, $type, $name);
    }
    else {
      $result = FALSE;
    }

    if ($result) {
      // Redirect to the edit version of this form
      $form_state->setRedirect('node_organizer.edit', ['collection' => $this->node_organizer->getCollectionId()]);
      drupal_set_message(t('Successfully saved collection %collection.', array('%collection' => $this->node_organizer->getName())));
    }
    else {
      drupal_set_message(t('There was an issue saving collection %collection', array('%collection' => $this->node_organizer->getName())));
    }
  }

  /**
   * Submit handler for removing a node/folder
   *
   * @param $form
   * @param $form_state
   */
  function removeItem(array &$form, FormStateInterface $form_state) {
    $node_organizer = $form_state->get('node_organizer');
    $this->node_organizer->loadCollection($node_organizer['collection_id']);

    // Get the ID to remove
    $trigger = $form_state->getTriggeringElement();
    $item_id = $trigger['#name'];

    // Recurse through the hierarchy and remove the item
    $this->utilities->recursiveRemove($node_organizer['collection'], $item_id);

    $name = $form_state->getValue('name');
    $type = $form_state->getValue('type');
    $result = $this->node_organizer->updateCollection($node_organizer['collection'], $type, $name);
    $form_state->set('node_organizer', $node_organizer);

    if ($result) {
      // Redirect to the edit version of this form
      $form_state->setRedirect('node_organizer.edit', ['collection' => $this->node_organizer->getCollectionId()]);
      drupal_set_message(t('Successfully saved collection %collection.', array('%collection' => $this->node_organizer->getName())));
    }
    else {
      drupal_set_message(t('There was an issue saving collection %collection', array('%collection' => $this->node_organizer->getName())));
    }
  }

  /**
   * Recurses through the collection data structure and creates the form items.
   *
   * @param $current_level
   *   The current level in the structure
   * @param $form
   *   The form
   * @param null $parent
   *   The parent ID if there is one
   */
  protected function listCreate($current_level, &$form, $parent = NULL) {
    if (is_array($current_level)) {
      // Iterate through the current level in the hierarchy
      foreach ($current_level as $key => $params) {
        // Set a bunch of values
        $href = NULL;
        $id = $params->getId();
        $type = $params->getType();
        if ($type == 'node') {
          $href = "node/$id";
        }
        $has_children = $params->hasChildren();

        // Create the form entry for this item
        $form[$id] = $this->formEntry(
          $type,
          $id,
          $params->getTitle(),
          $href,
          $parent,
          $params->depth,
          $params->weight,
          !$has_children
        );

        // If the item has children, go down a level
        if ($has_children) {
          $this->listCreate($params->children, $form, $id);
        }
      }
    }
  }

  /**
   * Creates a form entry with given parameters
   *
   * @param $type
   * @param $id
   * @param $title
   * @param null $href
   * @param null $parent
   * @param int $depth
   * @param int $weight
   * @param bool $remove
   * @return array
   */
  function formEntry($type, $id, $title, $href = NULL, $parent = NULL, $depth = 0, $weight = -50, $remove = TRUE) {
    $indentation = [
      '#theme' => 'indentation',
      '#size' => $depth,
    ];

    $item = array();

    if ($type == 'node') {
      $item['title'] = array(
        '#prefix' => $indentation > 0 ? $this->renderer->render($indentation) : '',
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $title,
      );
    } else {
      $item['title'] = array(
        '#prefix' => $indentation > 0 ? $this->renderer->render($indentation) : '',
        '#type' => 'textfield',
        '#size' => 15,
        '#maxlength' => 255,
        '#value' => $title,
      );
    }

    $item['hidden'] = array(
      'weight' => array(
        '#type' => 'textfield',
        '#default_value' => $weight,
        '#delta' => 50,
        '#title' => t('Weight for @title', array('@title' => $title)),
        '#title_display' => 'invisible',
        '#attributes' => array('class' => array('collection-weight')),
      ),
      'nid' => array(
        '#type' => 'hidden',
        '#value' => $type == 'node' ? $id : FALSE,
      ),
      'fid' => array(
        '#type' => 'hidden',
        '#value' => $type == 'folder' ? $id : FALSE,
      ),
      'plid' => array(
        '#type' => 'hidden',
        '#default_value' => $parent,
        '#attributes' => array('class' => array('collection-plid')),
      ),
      'mlid' => array(
        '#type' => 'hidden',
        '#default_value' => $id,
        '#attributes' => array('class' => array('collection-mlid')),
      ),
      'depth' => array(
        '#type' => 'hidden',
        '#default_value' => $depth,
      ),
      'href' => array(
        '#type' => 'hidden',
        '#value' => $href,
      ),
    );

    if ($type == 'node') {
      $item['operations_1'] = [
        '#type' => 'operations',
        '#links' => [
          'view' => [
            'title' => t('view'),
            'url' => Url::fromRoute('entity.node.canonical', ['node' => $id]),
          ],
          'edit' => [
            'title' => t('edit'),
            'url' => Url::fromRoute('entity.node.edit_form', ['node' => $id]),
          ],
        ],
      ];
    } else {
      $item['operations_1'] = array(
        '#type' => 'html_tag',
        '#tag' => 'span',
      );
    }

    $item['operations_2'] = array(
      '#type' => 'html_tag',
      '#tag' => 'span',
    );
    // Only add the Remove action if this item has no children ($remove == !$has_children)
    if ($remove) {
      $display_id = $id;
      if ($type == 'folder') {
        $display_id = $id * -1;
      }
      $item['operations_2'] = array(
        '#type' => 'submit',
        '#name' => $id,
        '#value' => "Remove $type ($display_id)",
        '#submit' => array('::removeItem'),
      );
    }

    $item['#attributes'] = array('class' => array('draggable'));

    $item['#weight'] = $weight;

    return $item;
  }

  /**
   * Recurses through form items to build the stored data structure.
   *
   * @param $sorter
   *   The list of submitted form items grouped by parent
   * @param $final_list
   *   The array that will be the final data structure
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param int $depth
   *   The current depth in the tree
   */
  protected function recurseFormItems(&$sorter, &$final_list, FormStateInterface $form_state, $depth = 1) {
    foreach ($final_list as $pid => $parent) {
      if (isset($sorter[$pid])) {
        foreach ($sorter[$pid] as $cid => $child) {
          // Set the depth (doesn't seem to come over properly in the form)
          $child->depth = $depth;
          // Add the child item
          $final_list[$pid]->children[$cid] = $child;
        }
        if ($final_list[$pid]->hasChildren()) {
          // Go through the next level
          $this->recurseFormItems($sorter, $final_list[$pid]->children, $form_state, $depth + 1);
        }
      }
//      elseif ($final_list[$pid]->getType() == 'folder') {
//        $form_state->setErrorByName("table][collection-folder-$pid", t('Empty folders are not allowed. Folders must have at least one node in them.'));
//      }
    }
  }
}
