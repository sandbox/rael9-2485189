<?php
/**
 * Created by IntelliJ IDEA.
 * User: jbaker
 * Date: 5/11/17
 * Time: 2:58 PM
 */

namespace Drupal\node_organizer\Form;


use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node_organizer\NodeOrganizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeOrganizerDeleteForm extends ConfirmFormBase {
  protected $node_organizer;

  public function __construct(NodeOrganizer $node_organizer) {
    $this->node_organizer = $node_organizer;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('node_organizer')
    );
  }

  public function getFormId() {
    return 'node_organizer_delete';
  }

  /**
   * Form for deleting collections.
   *
   * @param $form
   * @param $form_state
   * @param $collection
   *
   * @return bool|array
   */
  function buildForm(array $form, FormStateInterface $form_state, $collection = NULL) {
    if (is_numeric($collection)) {
      $this->node_organizer->loadCollection($collection);
      $form_state->set('node_organizer', $this->node_organizer->getCollectionId());
      return parent::buildForm($form, $form_state);
    }
    return FALSE;
  }

  public function getQuestion() {
    return $this->t('Are you sure you want to delete the "' . $this->node_organizer->getName() . '" collection?');
  }

  public function getCancelUrl() {
    return new Url('node_organizer.default', ['collection' => $this->node_organizer->getCollectionId()]);
  }

  /**
   * Submit handler for node_organizer_collection_delete().
   *
   * @param $form
   * @param $form_state
   */
  function submitForm(array &$form, FormStateInterface $form_state) {
    // Get some info, then delete the collection.
    $this->node_organizer->loadCollection($form_state->get('node_organizer'));
    $name = $this->node_organizer->getName();
    $type = $this->node_organizer->getType();
    $this->node_organizer->deleteCollection();

    // Set the message.
    drupal_set_message($this->t('The @name collection has been deleted.', array('@name' => $name)));

    // Redirect back to the appropriate list page based on type.
    if ($type == 'system') {
      $form_state->setRedirect('node_organizer.list');
    }
    elseif ($type == 'personal') {
      $form_state->setRedirect('node_organizer.my_collections');
    }
  }
}
