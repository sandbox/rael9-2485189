<?php
/**
 * @file
 *   Class for hierarchy manipulation
 */

namespace Drupal\node_organizer;

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\node_organizer\NodeOrganizerItem;
use Drupal\Core\Session\AccountInterface;


class NodeOrganizer {
  use DependencySerializationTrait;

  // The fallback default for type is personal, since it doesn't have system-wide implications.
  const DEFAULT_TYPE = 'personal';

  // The DB connection
  protected $connection;
  // Current user.
  protected $user;
  // The time this collection was updated.
  protected $updated;
  // The user ID of the creator of this collection.
  protected $uid;
  // The name of this collection (only really used for identification).
  protected $name = '';
  // The unique ID (noid) of this collection.
  protected $collectionId = NULL;
  // The actual hierarchical collection.
  protected $collection = NULL;
  // The available types.
  protected $types = array('system', 'personal');
  // The type of this collection.
  protected $type = NULL;
  // The number of items in this collection.
  protected $count = 0;
  // The nodes included in this collection.
  protected $nodes = array();
  // The currently loaded node_organizer child object.
  protected $currentObject = NULL;
  // The previous object in the hierarchy.
  protected $previous = NULL;
  // The next object in the hierarchy.
  protected $next = NULL;
  // The top-level (or first) object.
  protected $top = NULL;

  function __construct(Connection $connection, AccountInterface $user) {
    $this->connection = $connection;
    $this->user = $user;
  }

  /**
   * Start Getters and Setters
   */

  /**
   * @return int
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * @param int $uid
   */
  public function setUid($uid) {
    $this->uid = $uid;
  }

  /**
   * @return int
   */
  public function getUpdated() {
    return $this->updated;
  }

  /**
   * @param int $updated
   */
  public function setUpdated($updated) {
    $this->updated = $updated;
  }

  /**
   * @return string
   */
  public function getName() {
    return Html::decodeEntities($this->name);
  }

  /**
   * @param string $name
   */
  protected function setName($name) {
    $this->name = $name;
  }

  /**
   * @return int
   */
  public function getCollectionId() {
    return $this->collectionId;
  }

  /**
   * @param int $collectionId
   */
  public function setCollectionId($collectionId) {
    $this->collectionId = $collectionId;
  }

  /**
   * @return array
   */
  public function getCollection() {
    return $this->collection;
  }

  /**
   * @param array $collection
   */
  protected function setCollection($collection) {
    $this->collection = $collection;
  }

  /**
   * @return array
   */
  public function getTypes() {
    return $this->types;
  }

  /**
   * @param array $types
   */
  protected function setTypes($types) {
    $this->types = $types;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return int
   */
  public function getCount() {
    return $this->count;
  }

  /**
   * @param int $count
   */
  protected function setCount($count) {
    $this->count = $count;
  }

  /**
   * @return array
   */
  public function getNodes() {
    return $this->nodes;
  }

  /**
   * @param array $nodes
   */
  protected function setNodes($nodes) {
    $this->nodes = $nodes;
  }

  /**
   * @return NodeOrganizerItem
   */
  public function getCurrentObject() {
    return $this->currentObject;
  }

  /**
   * @param $id
   */
  public function setCurrentObject($id) {
    // Load the object by id and set it.
    $this->loadObject($id, 'object', $object);
    $this->currentObject = $object;
  }

  /**
   * Get the previous object in line from the requested or current object
   *
   * @return NodeOrganizerItem
   */
  public function getPrevious() {
    // If the previous item hasn't been set yet, set it. Then return it.
    if ($this->previous === NULL) {
      $this->setPrevious();
    }
    return $this->previous;
  }


  /**
   * @param null $object
   * @return bool
   * @internal param null $previous
   */
  public function setPrevious($object = NULL) {
    // If no object is specified, load the current object.
    if (!$object) {
      $object = $this->getCurrentObject();
    }

    // Get the siblings of the object, and iterate the pointer to the object.
    $siblings = $this->loadSiblings($object);
    while (key($siblings) != $object->getId()) {
      next($siblings);
    }

    // If there is a previous sibling, figure out if it or a child of it should be the previous item.
    if ($value = prev($siblings)) {
      // Iterate down to the bottom of the children of the last items in the hierarchy.
      while ($value->hasChildren()) {
        $value = end($value->children);
      }
      $this->previous = $value;
      return FALSE;
    }

    // If there was no previous sibling, we'll go up to the parent and check it.
    if ($value = $this->loadParent($object)) {
      // If the parent is a folder, we need to recurse to get a valid node to return.
      if ($value->getType() == 'folder') {
        $this->setPrevious($value);
      }
      else {
        $this->previous = $value;
      }
      return FALSE;
    }

    // If there was no next item, set it to FALSE.
    if ($this->previous === NULL) {
      $this->previous = FALSE;
    }
    return TRUE;
  }

  /**
   * Gets the next object in line from the requested or current object
   * @return NodeOrganizerItem
   */
  public function getNext() {
    // If the next item hasn't been set yet, set it. Then return it.
    if ($this->next === NULL) {
      $this->setNext();
    }
    return $this->next;
  }

  /**
   * @param null $object
   * @param bool $parent
   *
   * @return bool
   * @internal param null $next
   */
  protected function setNext($object = NULL, $parent = FALSE) {
    // If no object is specified, load the current object.
    if (!$object) {
      $object = $this->getCurrentObject();
    }

    // Get the siblings of the object, and iterate the pointer to the object.
    $siblings = $this->loadSiblings($object);
    while (key($siblings) != $object->getId()) {
      next($siblings);
    }

    // If there are children, and this isn't a parent recurse, iterate down the first items until we hit a node.
    if ($object->hasChildren() && !$parent) {
      $value = reset($object->children);
      while ($value->getType() == 'folder') {
        $value = reset($value->children);
      }
      $this->next = $value;
      return FALSE;
    }

    // If there is a sibling after this object, iterate down the first items until we hit a node.
    if ($value = next($siblings)) {
      while ($value->getType() == 'folder') {
        $value = reset($value->children);
      }
      $this->next = $value;
      return FALSE;
    }

    // If there was no next sibling, get the parent and try getting the next object from there.
    if ($parent = $this->loadParent($object)) {
      $this->setNext($parent, TRUE);
    }

    // If there was no next item, set it to FALSE.
    if ($this->previous === NULL) {
      $this->next = FALSE;
    }
    return TRUE;
  }

  /**
   * @return NodeOrganizerItem
   */
  public function getTop() {
    // If the top item hasn't been set yet, set it. Then return it.
    if ($this->top === NULL) {
      $this->setTop();
    }
    return $this->top;
  }

  /**
   * Sets the top item based on the collection hierarchy
   */
  protected function setTop() {
    // Load the collection.
    $collection = $this->getCollection();
    // We want the first item in the collection.
    $first = reset($collection);
    $this->top = $first;
  }

  /**
   * End Getters and Setters
   */

  /**
   * Loads a collection based on the noid set in collectionId
   * @param null $noid
   * @param bool $break_cache
   * @return mixed
   */
  public function loadCollection($noid, $break_cache = FALSE) {
    // TODO: Fix the caching here.
    // If we aren't breaking the cache on purpose, try pulling the collection info from cache.
    // if (!$break_cache) {
    //   $collection = &drupal_static(__METHOD__ . '_collection');
    //   $type = &drupal_static(__METHOD__ . '_type');
    //   $name = &drupal_static(__METHOD__ . '_name');
    //   $noid = &drupal_static(__METHOD__ . '_noid');
    //   $uid = &drupal_static(__METHOD__ . '_uid');
    // }
    // Default everything to NULL if this collection isn't loadable.
    $collection = NULL;
    $type = NULL;
    $name = NULL;
    $uid = NULL;
    $updated = NULL;

    // If the cache pull wasn't done, or failed, pull the data from the DB.
    if ($collection === NULL) {
      // If we have a noid, grab the collection from the DB.
      if ($noid !== NULL) {
        $select = $this->connection->select('node_organizer', 'no');
        $select->condition('noid', $noid, '=');
        $select->fields('no', array('uid', 'name', 'collection', 'type', 'updated'));
        $query = $select->execute();
        $result = $query->fetchAssoc();
        // Unserialize the stored array.
        $collection = unserialize($result['collection']);
        $type = $result['type'];
        $name = $result['name'];
        $uid = $result['uid'];
        $updated = $result['updated'];

        // Get a list of nodes stored in this collection.
        $nodes = $this->getNodeList($collection);
        // Set this list as a property, and the count of nodes.
        $this->setNodes($nodes);
        $this->setCount(count($nodes));
      }
      else {
        \Drupal::logger('node_organizer')->notice('Tried to load a non-existent node collection.');
      }
    }

    // Set the local properties with the gathered values.
    $this->setCollectionId($noid);
    $this->setCollection($collection);
    $this->setType($type);
    $this->setName($name);
    $this->setUid($uid);
    $this->setUpdated($updated);

    // Return the collection so a separate call to getCollection() is unneeded.
    return $collection;
  }

  /**
   * Deletes the currently loaded collection
   */
  public function deleteCollection() {
    if ($id = $this->getCollectionId()) {
      $this->connection->delete('node_organizer')
        ->condition('noid', $id, '=')
        ->execute();

      $this->connection->delete('node_organizer_nodes')
        ->condition('noid', $id, '=')
        ->execute();
    }
  }

  /**
   * Get the array of siblings for the requested or current object
   *
   * @param null $object
   * @return array
   */
  public function loadSiblings($object = NULL) {
    // Load the current object if it wasn't passed in.
    if (!$object) {
      $object = $this->getCurrentObject();
    }
    // Load the list of siblings and return it.
    $this->loadObject($object->getId(), 'siblings', $siblings);
    return $siblings;
  }

  /**
   * Get the parent object of the requested or current object
   *
   * @param null $object
   * @return bool|mixed|null
   */
  public function loadParent($object = NULL) {
    // Load the current object if it wasn't passed in.
    if (!$object) {
      $object = $this->getCurrentObject();
    }
    // If the object has a parent, load it and return it.
    $parent = FALSE;
    if ($object->hasParent()) {
      $this->loadObject($object->parent, 'object', $parent);
    }
    return $parent;
  }

  /**
   * Adds the collection to the DB
   *
   * @param $collection
   * @param $type
   * @param $name
   * @param null $uid
   * @return bool
   * @throws \Exception
   */
  public function newCollection($collection, $type, $name, $uid = NULL) {
    if (!$uid) {
      $uid = $this->user->id();
    }

    // Do a check on the incoming collection data.
    $this->checkCollection($collection);
    $name = Html::escape($name);
    $types = $this->getTypes();

    // Do some basic data validation checks.
    if (is_array($collection) && in_array($type, $types)) {
      // Get the list of nodes included in this collection data.
      $nodes = $this->getNodeList($collection);
      // Serialize the collection for storage.
      $raw_collection = serialize($collection);

      // Insert the collection into the DB.
      $noid = $this->connection->insert('node_organizer')
        ->fields(
          array('uid', 'name', 'collection', 'type', 'updated'),
          array($uid, $name, $raw_collection, $type, time())
        )
        ->execute();

      // Insert the node to collection associations.
      $insert = $this->connection->insert('node_organizer_nodes')
        ->fields(array('noid', 'nid'));
      foreach ($nodes as $nid) {
        $insert->values(array($noid, $nid));
      }
      $insert->execute();

      // Load this collection for use.
      $this->loadCollection($noid, TRUE);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Updates the collection in the DB
   *
   * @param $collection
   * @param $type
   * @param $name
   *
   * @return bool
   */
  public function updateCollection($collection, $type, $name) {
    // Do a check on the incoming collection data.
    $this->checkCollection($collection);
    $name = Html::escape($name);
    $noid = $this->getCollectionId();
    $types = $this->getTypes();
    $uid = $this->user->id();
    $updated = time();

    // Do some basic data validation checks.
    if (is_array($collection) && in_array($type, $types)) {
      // Get the list of nodes included in this collection data.
      $nodes = $this->getNodeList($collection);
      // Serialize the collection for storage.
      $raw_collection = serialize($collection);

      //Update the collection in the DB.
      $this->connection->update('node_organizer')
        ->fields(array('uid' => $uid, 'name' => $name, 'collection' => $raw_collection, 'type' => $type, 'updated' => $updated))
        ->condition('noid', $noid, '=')
        ->execute();

      // Get the list of nodes that currently belong to the collection.
      $select = $this->connection->select('node_organizer_nodes', 'non');
      $select->condition('noid', $noid, '=');
      $select->fields('non', array('nid'));
      $results = $select->execute();
      $current_nodes = array();
      while ($result = $results->fetchAssoc()) {
        $current_nodes[] = $result['nid'];
      }

      // Create the list of items that need to be added/deleted by comparing the new list to what's currently in the DB.
      $deletes = array_diff($current_nodes, $nodes);
      $adds = array_diff($nodes, $current_nodes);

      // Delete nodes that were removed.
      foreach ($deletes as $delete) {
        $this->connection->delete('node_organizer_nodes')
          ->condition('nid', $delete, '=')
          ->condition('noid', $noid, '=')
          ->execute();
      }

      // Insert nodes that were added.
      if (count($adds)) {
        $insert = $this->connection->insert('node_organizer_nodes')
          ->fields(array('noid', 'nid'));
          foreach ($adds as $nid) {
            $insert->values(array($noid, $nid));
          }
          $insert->execute();
      }

      // Re-load the collection with the new data.
      $this->loadCollection($noid, TRUE);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Callback for sorting by weight
   *
   * @param $first
   * @param $second
   * @return int
   */
  protected function compareSiblingWeight ($first, $second)  {
    if (intval($first->weight) == intval($second->weight)) {
      return 0;
    }
    if (intval($first->weight) > intval($second->weight)) {
      return 1;
    }
    else {
      return -1;
    }
  }

  /**
   * Recurses through the object checking the data (currently just sorts by weight)
   *
   * @param $collection
   */
  protected function checkCollection(&$collection) {
    if (is_array($collection)) {
      // Sort using the weights stored in the collection objects.
      uasort($collection, array($this, 'compareSiblingWeight'));
      // If the object has children, go down a level.
      foreach ($collection as $item) {
        if ($item->hasChildren()) {
          $this->checkCollection($item->children);
        }
      }
    }
  }

  /**
   * Recurses through the collection and returns the required subset
   *
   * @param $id
   * @param $subset
   * @param $return
   * @param null $current_level
   */
  protected function loadObject($id, $subset, &$return, $current_level = NULL) {
    // Load the current collection if it wasn't passed in.
    if ($current_level === NULL) {
      $current_level = $this->getCollection();
    }
    // If this is a valid list of siblings, try to find the current object in the list.
    if (is_array($current_level)) {
      if (isset($current_level[$id])) {
        // Return the object or the list of it and it's siblings, as requested.
        switch ($subset) {
          case 'object':
            $return = $current_level[$id];
            break;
          case 'siblings':
            $return = $current_level;
            break;
        }
      }
      else {
        // If we didn't find it at this level, iterate.
        foreach ($current_level as $params) {
          // If the object has children, go down a level.
          if ($params->hasChildren()) {
            $this->loadObject($id, $subset, $return, $params->children);
          }
        }
      }
    }
  }

  /**
   * Recurses through the collection and gets all of the nids
   *
   * @param $current_level
   * @param array $nodes
   * @return array
   */
  protected function getNodeList($current_level, &$nodes = array()) {
    if (is_array($current_level)) {
      foreach ($current_level as $id => $params) {
        // Add the nid to the list, if it exists.
        if ($params->getType() == 'node') {
          $nodes[] = $params->nid;
        }
        // If the object has children, go down a level.
        if ($params->hasChildren()) {
          $this->getNodeList($params->children, $nodes);
        }
      }
    }

    return $nodes;
  }

  /**
   * Looks up what collections a node belongs to based on nid and type
   *
   * @param $nid
   * @param null $type
   * @return array|bool
   */
  public function nodeLookup($nid, $type = NULL) {
    // TODO: fix the caching here.
    // Grab this from cache, if cached.
    // $collections = &drupal_static(__METHOD__ . "_$nid" . "_$type");
    // If not, load it from the DB.
    if (!isset($collections)) {
      $collections = array();

      // Select and collections where this node is a member.
      $select = $this->connection->select('node_organizer_nodes', 'non');
      // If the type was passed in, join to the collections table to filter by type.
      if ($type !== NULL) {
        $select->join('node_organizer', 'no', 'non.noid = no.noid');
        $select->condition('type', $type, '=');
      }
      $select->condition('nid', $nid, '=');
      $select->fields('non', array('noid'));
      $query = $select->execute();

      // Grab the list of collections.
      while ($result = $query->fetchAssoc()) {
        $collections[] = $result['noid'];
      }
    }

    // Return FALSE if there weren't any matches.
    if (count($collections)) {
      return $collections;
    }
    return FALSE;
  }

  /**
   * Recurses over the collection to get the lowest folder ID.
   *
   * @param int $fid
   * @return int|string
   */
  public function nextFolder($collection = FALSE, &$fid = 0) {
    if (!$collection) {
      $collection = $this->collection;
    }
    if (count($collection)) {
      foreach ($collection as $id => $item) {
        if ($item->hasChildren()) {
          $this->nextFolder($item->children, $fid);
        }
        if (intval($id) < $fid) {
          $fid = $id;
        }
      }
    }
    return $fid - 1;
  }
}
