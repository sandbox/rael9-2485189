<?php
/**
 * Created by IntelliJ IDEA.
 * User: jbaker
 * Date: 5/10/17
 * Time: 4:18 PM
 */

namespace Drupal\node_organizer;

use Drupal\node\Entity\Node;

class NodeOrganizerItem {
  // The title set for this object (not necessarily the same as the node title).
  public $title = NULL;
  // The nid if this is a node.
  public $nid = NULL;
  // The fid if this is a folder.
  public $fid = NULL;
  // The array of child objects.
  public $children = array();
  // The weight in the hierarchy.
  public $weight = 0;
  // The depth in the hierarchy.
  public $depth = 0;
  // The id of the parent, if there is one.
  public $parent = NULL;

  public function getTitle() {
    if ($this->nid !== NULL) {
      $node = Node::load($this->nid);
      return $node->getTitle();
    }
    return $this->title;
  }

  /**
   * Returns the ID, regardless of type.
   *
   * @return mixed
   */
  public function getId() {
    return $this->nid !== NULL ? $this->nid : $this->fid;
  }

  /**
   * Returns the type, 'node' or 'folder'.
   *
   * @return string
   */
  public function getType() {
    return $this->nid !== NULL ? 'node' : 'folder';
  }

  /**
   * Whether this object has children or not.
   *
   * @return bool
   */
  public function hasChildren() {
    return count($this->children) ? TRUE : FALSE;
  }

  /**
   * Whether this object has a parent.
   *
   * @return bool
   */
  public function hasParent() {
    return $this->parent ? TRUE : FALSE;
  }
}
