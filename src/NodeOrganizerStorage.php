<?php
/**
 * Created by IntelliJ IDEA.
 * User: jbaker
 * Date: 5/10/17
 * Time: 1:12 PM
 */

namespace Drupal\node_organizer;

use Drupal\Core\Database\Connection;
use Drupal\Component\Utility\Html;


class NodeOrganizerStorage {
  protected $connection;

  function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Gets a list of collections by type or user ID. Returns all collections if
   * neither filter is supplied.
   *
   * @param array $filters
   *   column => value pairs of filters.
   * @return array
   */
  public function getCollectionList($filters = array()) {
    $collections = array();

    // Select any collections with the given type.
    $select = $this->connection->select('node_organizer', 'no');
    // Add filters as needed.
    foreach ($filters as $column => $value) {
      $select->condition($column, $value, '=');
    }
    $select->fields('no', array('noid', 'name'));
    $query = $select->execute();

    // Grab the list of collections.
    while ($result = $query->fetchAssoc()) {
      $collections[$result['noid']] = Html::decodeEntities($result['name']);
    }

    return $collections;
  }
}
