<?php
/**
 * Created by IntelliJ IDEA.
 * User: jbaker
 * Date: 5/11/17
 * Time: 10:39 AM
 */

namespace Drupal\node_organizer;

use Drupal\Core\Link;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\node\Entity\Node;
use Drupal\node_organizer\NodeOrganizer;
use Drupal\node_organizer\NodeOrganizerStorage;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeOrganizerUtilities {
  protected $storage;
  protected $nodeOrganizer;
  protected $moduleHandler;

  public function __construct(NodeOrganizerStorage $storage, NodeOrganizer $nodeOrganizer, ModuleHandler $moduleHandler) {
    $this->storage = $storage;
    $this->nodeOrganizer = $nodeOrganizer;
    $this->moduleHandler = $moduleHandler;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('node_organizer.storage'),
      $container->get('node_organizer'),
      $container->get('module_handler')
    );
  }

  /**
   * Creates renderable list of collections.
   *
   * @param $node
   *
   * @return array
   **/
  public function addToList(Node $node) {
    $account = \Drupal::currentUser();

    // We only want to run this if this is a logged-in user.
    if ($account->isAuthenticated()) {
      // Get the list of collections for this user.
      $collections = $this->storage->getCollectionList(['uid' => $account->id(), 'type' => 'personal']);

      // Get a list of collections to which this node belongs.
      $nid = $node->id();
      $node_collections = $this->nodeOrganizer->nodeLookup($nid);

      // Common list settings.
      $list = [
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#attributes' => [
          'id' => 'node-organizer-add-to-list-' . $account->id(),
          'class' => ['node-organizer-add-to-list'],
        ],
        '#items' => [],
      ];

      // Add each collection to the renderable list.
      foreach ($collections as $noid => $name) {
        // If this node is in this collection, make it a removal link.
        if (in_array($noid, $node_collections)) {
          $data = Link::createFromRoute($name, 'node_organizer.node_remove', ['collection' => $noid, 'node' => $nid])->toString()->getGeneratedLink();
          $class = 'active';
        }
        // Otherwise make it an add link.
        else {
          $data = Link::createFromRoute($name, 'node_organizer.node_add', ['collection' => $noid, 'node' => $nid])->toString()->getGeneratedLink();
          $class = 'inactive';
        }

        // Create the list item
        $list['#items'][] = [
          '#markup' => $data,
          '#wrapper_attributes' => [
            'class' => [
              $class,
            ],
          ],
        ];
      }

      // Add a list item for creating a new collection to add this node to.
      $list['#items'][] = [
        '#markup' => Link::createFromRoute(t('Add to New Collection'), 'node_organizer.node_add', ['collection' => 'new', 'node' => $nid])->toString()->getGeneratedLink(),
        '#wrapper_attribute' => [
          'class' => [
            'new',
          ],
        ],
      ];

      // @TODO: See if we can set up a good sane caching setup instead of not caching this block at all.
      // Build the block with the needed CSS/JS and return it.
      return [
        'subject' => NULL,
        'content' => [
          'list' => $list,
          '#attached' => [
            'library' => [
              'node_organizer/add-to-list-block'
            ],
          ],
        ],
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }
    return [];
  }

  /**
   * Recurses through the collection creating the tree navigation list items.
   *
   * @hook hook_node_organizer_display_alter()
   *   Devs can use this hook to customize the display of the list items. The
   *   list item with default values, the object for each leaf in the tree, and
   *   the numerical place within the current level in the hierarchy are passed to
   *   the hook.
   *
   *   The list item is pre-populated with 'data' and 'class'. 'data' should be
   *   the pre-rendered HTML you want to display for this item.
   *
   *   The object includes the following properties:
   *     $title - The title set in the collection (not necessarily the node title).
   *     $nid - The node ID if this is a node.
   *     $fid - The folder ID if this is a folder.
   *     $children - An array of child objects.
   *     $weight - The weight within the hierarchy.
   *     $depth - The depth in the hierarchy.
   *     $parent - The ID of the parent object.
   *
   *   And the following methods:
   *     getId() - Returns the ID, regardless of type.
   *     getType() - The object type ('node' or 'folder').
   *     hasChildren() - Whether this object has child objects.
   *     hasParent() - Whether this object has a parent object.
   *
   *   The numerical value is passed in case you want to override the titles with
   *   automatically numbered values.
   *
   *   The hook should return rendered HTML for the item in the hierarchy.
   *
   * @param $current_level
   * @param $list
   * @param int $current_node
   */
  public function recurseCollection($current_level, &$list, $current_node = 0) {
    if (is_array($current_level)) {
      // Counter for determining numerical place in the current level of hierarchy.
      $item_num = 1;

      // Go through each object at this level.
      foreach($current_level as $id => $item) {
        // Create the default simple text (with or without link).
        if ($item->getType() == 'node') {
          $data = Link::createFromRoute($item->getTitle(), 'entity.node.canonical', ['node' => $item->nid])->toString()->getGeneratedLink();
        }
        else {
          $data = $item->getTitle();
        }

        $classes = array('node-organizer-depth-' . $item->depth);
        if ($item->hasChildren()) {
          $classes[] = 'node-organizer-tree-branch';
        }
        else {
          $classes[] = 'node-organizer-tree-leaf';
        }
        if ($item->nid) {
          $classes[] = 'node-organizer-node';
        } else {
          $classes[] = 'node-organizer-folder';
        }
        if ($item->nid == $current_node) {
          $classes[] = 'node-organizer-active';
        }
        // Add this item to the list.
        $list[] = array(
          '#markup' => $data,
          '#wrapper_attributes' => array(
            'class' => $classes,
          ),
        );

        // Calculate the key of the item that was just added.
        $key = count($list) - 1;

        // Call hook_node_organizer_display_alter() with the object so each item's display is customizable.
        $this->moduleHandler->alter('node_organizer_display', $list[$key], $item, $item_num);

        // Iterate the counter.
        $item_num++;

        // If this item has children, add an array for the children, and recurse.
        if ($item->hasChildren()) {
          $list[$key]['children'] = array();
          $this->recurseCollection($item->children, $list[$key]['children'], $current_node);
        }
      }
    }
  }

  /**
   * Recurses through the collection to remove the item
   *
   * @param $collection
   * @param $remove
   */
  public function recursiveRemove(&$collection, $remove) {
    foreach ($collection as $id => $item) {
      if ($item->hasChildren()) {
        $this->recursiveRemove($item->children, $remove);
      }
      if ($id == $remove) {
        unset($collection[$id]);
      }
    }
  }

  /**
   * Creates the data structure for a collection export that can be converted to
   * JSON (or other formats).
   *
   * @param $collection
   *
   * @return array
   */
  public function getCollectionsExport($collection) {
    $json_collections = [];
    $filters = ['type' => 'system'];
    if ($collection) {
      $filters['noid'] = $collection;
    }
    $collections = $this->storage->getCollectionList($filters);

    foreach ($collections as $noid => $name) {
      $this->nodeOrganizer->loadCollection($noid);
      $collection_user = User::load($this->nodeOrganizer->getUid());
      $json_collections[$noid] = [
        'type' => $this->nodeOrganizer->getType(),
        'name' => $this->nodeOrganizer->getName(),
        'user_id' => $this->nodeOrganizer->getUid(),
        'user_uuid' => $collection_user->uuid(),
        'updated' => $this->nodeOrganizer->getUpdated(),
        'structure' => [],
      ];
      $this->recurseCollectionStructure($this->nodeOrganizer->getCollection(), $json_collections[$noid]['structure']);
    }

    return $json_collections;
  }

  /**
   * Recurses through the collection structure to convert it to an exportable
   * data structure.
   *
   * @param $current_level
   * @param $list
   */
  private function recurseCollectionStructure($current_level, &$list) {
    if (is_array($current_level)) {
      // Go through each object at this level.
      $list_item = [];
      foreach($current_level as $id => $item) {
        $list_item['type'] = $item->getType();
        $list_item['title'] = $item->getTitle();
        $list_item['id'] = $item->getId();
        $list_item['weight'] = $item->weight;
        $list_item['depth'] = $item->depth;
        $list_item['parent'] = $item->parent;

        // Load the UUID of the parent.
        if ($item->hasParent() && $item->parent > 0) {
          $node = Node::load($item->parent);
          $list_item['parent_uuid'] = $node->uuid();
        }

        // If this is a node, get the UUID for this node and add it.
        if ($item->nid) {
          $node = Node::load($item->nid);
          $list_item['uuid'] = $node->uuid();
        }

        // If this item has children, add an array for the children, and recurse.
        if ($item->hasChildren()) {
          $list_item['children'] = [];
          $this->recurseCollectionStructure($item->children, $list_item['children']);
        }

        $list[] = $list_item;
      }
    }
  }
}
