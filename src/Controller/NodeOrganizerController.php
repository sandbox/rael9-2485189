<?php
/**
 * Created by IntelliJ IDEA.
 * User: jbaker
 * Date: 5/10/17
 * Time: 11:07 AM
 */

namespace Drupal\node_organizer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node_organizer\NodeOrganizer;
use Drupal\node_organizer\NodeOrganizerItem;
use Drupal\node_organizer\NodeOrganizerStorage;
use Drupal\node_organizer\NodeOrganizerUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class NodeOrganizerController extends ControllerBase {
  protected $node_organizer;
  protected $storage;
  protected $utilities;

  public function __construct(NodeOrganizer $node_organizer, NodeOrganizerStorage $storage, NodeOrganizerUtilities $utilities) {
    $this->node_organizer = $node_organizer;
    $this->storage = $storage;
    $this->utilities = $utilities;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('node_organizer'),
      $container->get('node_organizer.storage'),
      $container->get('node_organizer.utilities')
    );
  }

  public function getTitle($collection) {
    $this->node_organizer->loadCollection($collection);

    return $this->node_organizer->getName();
  }

  public function getCollection($collection) {
    $this->node_organizer->loadCollection($collection);

    // Common list settings.
    $list = [
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#attributes' => [
        'id' => 'node-organizer-tree-nav-' . $this->node_organizer->getCollectionId(),
        'class' => [
          'node-organizer-tree-nav',
        ],
      ],
      '#items' => [],
      '#attached' => [
        'library' => [
          'node_organizer/tree-nav-block',
        ],
      ],
    ];

    // Recurse through the collection, creating the tree in a list.
    $this->utilities->recurseCollection($this->node_organizer->getCollection(), $list['#items']);

    return $list;
  }

  /**
   * Build the table of system collections.
   *
   * @param $type
   *
   * @return array
   */
  public function collectionList($type) {
    $account = \Drupal::currentUser();

    $table = [
      '#theme' => 'table',
      '#header' => [t('Collection'), t('Operations')],
      '#sticky' => TRUE,
      '#empty' => t("There are no $type collections. You can add one using the link above."),
    ];

    $filters = ['type' => $type];
    if ($type == 'personal') {
      $filters['uid'] = $account->id();
    }
    $collections = $this->storage->getCollectionList($filters);

    foreach ($collections as $id => $name) {
      $operations = [
        '#type' => 'dropbutton',
        '#links' => [
          'edit' => [
            'title' => $this->t('edit'),
            'url' => Url::fromRoute('node_organizer.edit', ['collection' => $id]),
          ],
          'delete' => [
            'title' => $this->t('delete'),
            'url' => Url::fromRoute('node_organizer.delete', ['collection' => $id]),
          ],
        ],
      ];
      $table['#rows'][$id] = [
        'collection' => Link::createFromRoute($name, 'node_organizer.collection', ['collection' => $id]),
        'operations' => render($operations),
      ];
    }

    return $table;
  }

  /**
   * Default page information for collections.
   */
  public function pageDefault() {
    $account = \Drupal::currentUser();

    $default_message = '<p>' . t('This page is a placeholder until I have something to put here. Please see the links below.') . '</p>';

    $default_links = array();
    if ($account->hasPermission('edit node organizer system collections')) {
      $default_links[] = Link::createFromRoute(t('System Collections'), 'node_organizer.list');
    }
    if ($account->hasPermission('edit node organizer collections')) {
      $default_links[] = Link::createFromRoute(t('Add a Collection'), 'node_organizer.add');
    }

    $build['default_message'] = array(
      '#theme' => 'item_list',
      '#prefix' => '<div id="node-organizer-page">' . $default_message,
      '#suffix' => '</div>',
      '#items' => $default_links,
    );
    return $build;
  }

  /**
   * Handles addition of a node to a collection.
   *
   * @param $collection
   * @param $node
   *
   * @return bool|\Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function nodeAdd($collection, Node $node) {
    if (is_object($node)) {
      // Create a new object to put into the hierarchy.
      $object = new NodeOrganizerItem();
      $object->nid = $node->id();
      $object->depth = 0;
      $title = $node->getTitle();

      if ($collection == 'new') {
        // Since this is a new collection, this will be the first item. Make the weight the lowest value.
        $object->weight = -50;
        // Create a new collection and add the object to it.
        $this->node_organizer->newCollection(array($object->getId() => $object), 'personal', $title);
        // Set the message which will be returned.
        $message = $this->t('@title was added to new collection !collection.', array('@title' => $title, '@collection' => $title));
      }
      elseif (is_numeric($collection)) {
        // Since this is an existing collection, make the weight push it to the bottom of the list.
        $object->weight = 50;
        // Load the existing collection.
        $hierarchy = $this->node_organizer->loadCollection($collection);
        // Add this node into the hierarchy.
        $hierarchy[$object->getId()] = $object;
        // Update the hierarchy.
        $this->node_organizer->updateCollection($hierarchy, 'personal', $this->node_organizer->getName());
        // Set the message which will be returned.
        $message = $this->t('@title was added to collection @collection.', array('@title' => $title, '@collection' => $this->node_organizer->getName()));
      }
      else {
        // Set an error message to be returned, and watchdog in the case of an error.
        $message = $this->t('There was an error adding this item to your collection.');
        \Drupal::logger('node_organizer')->error(t('Bad collection ID'));
      }

      // If this is an AJAX request, rather than a direct request, set up and return JSON.
      if (isset($_GET['json'])) {
        // Get the updated block and render it.
        $list = $this->utilities->addToList($node);
        $rendered_list = render($list);

        // Create the JSON with the message and rendered block.
        $json = array('message' => '<li class="message">' . $message . '</li>', 'list' => $rendered_list);
        // Output the JSON.
        return new JsonResponse($json);
      }
      // If this is just a regular request, just set the message and return to the node.
      else {
        drupal_set_message($message);
        return new RedirectResponse(Url::fromRoute('entity.node.canonical', ['node' => $node->id()]));
      }
    }
    else {
      // Log if someone requested a bad node ID.
      \Drupal::logger('node_organizer')->notice(t('Bad node ID'));
    }
    return FALSE;
  }

  /**
   * Handles removal of a node from a collection.
   *
   * @param $collection
   * @param $node
   *
   * @return bool|\Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function nodeRemove($collection, Node $node) {
    if (is_object($node)) {
      if (is_numeric($collection)) {
        // As long as we have a (presumably) valid node and collection ID, load the collection.
        $hierarchy = $this->node_organizer->loadCollection($collection);
        // Set the current node as the current object, and get the actual object.
        $this->node_organizer->setCurrentObject($node->id());
        $object = $this->node_organizer->getCurrentObject();

        // If this object has children, we don't want to remove it.
        if ($object->hasChildren()) {
          // Set the message that will be returned.
          $message = $this->t("This item has children. It can't be removed.");
        }
        // As long as it doesn't have children, remove it from the hierarchy.
        else {
          // Removal from hierarchy.
          $this->utilities->recursiveRemove($hierarchy, $object->getId());
          // Update with the new hierarchy.
          $this->node_organizer->updateCollection($hierarchy, 'personal', $this->node_organizer->getName());
          // Set the message that will be returned.
          $message = $this->t('@title was removed from collection @collection.', array('@title' => $node->getTitle(), '@collection' => $this->node_organizer->getName()));
        }
      }
      else {
        // Send back an error message if something is awry and log it.
        $message = $this->t('There was an error adding this item to your collection.');
        \Drupal::logger('node_organizer')->error($this->t('Bad collection ID'));
      }

      // If this is an AJAX request, rather than a direct request, set up and return JSON.
      if (isset($_GET['json'])) {
        // Get the updated block and render it.
        $list = $this->utilities->addToList($node);
        $rendered_list = render($list);
        // Create the JSON with the message and rendered block.
        $json = array('message' => '<li class="message">' . $message . '</li>', 'list' => $rendered_list);
        // Output the JSON.
        return new JsonResponse($json);
      }
      // If this is just a regular request, just set the message and return to the node.
      else {
        drupal_set_message($message);
        return new RedirectResponse(Url::fromRoute('entity.node.canonical', ['node' => $node->nid]));
      }
    }
    else {
      // Log if someone requested a bad node ID.
      \Drupal::logger('node_organizer')->notice($this->t('Bad node ID'));
    }
    return FALSE;
  }

  /**
   * Checks access to a collection.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param $collection
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(AccountInterface $account, $collection) {
    // If this is an automated new collection creation, we want it to pass as long as users can edit.
    if ($collection == 'new') {
      return AccessResult::allowedIf($account->hasPermission('edit node organizer collections'));
    }

    // If it's an existing collection, we need to be more granular.
    $this->node_organizer->loadCollection($collection);
    $type = $this->node_organizer->getType();
    switch ($type) {
      // If it's a personal collection, it passes as long as they can edit, and it is theirs.
      case 'personal':
        if ($this->node_organizer->getUid() == $account->id()) {
          return AccessResult::allowedIf($account->hasPermission('edit node organizer collections'));
        }
        return AccessResult::forbidden();
      // If it's a system collection, it only passes if they have the system collection edit rights.
      case 'system':
        return AccessResult::allowedIf($account->hasPermission('edit node organizer system collections'));
    }

    return AccessResult::forbidden();
  }

  /**
   * Returns the collection structure as a JSON response.
   *
   * @param bool|int $collection Collection noid if loading single.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function collectionJson($collection = FALSE) {
    $json_collections = $this->utilities->getCollectionsExport($collection);

    return new JsonResponse($json_collections);
  }


}
